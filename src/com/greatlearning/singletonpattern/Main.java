package com.greatlearning.singletonpattern;

import java.sql.Connection;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {
        try {
            Connection conn = FetchConnection.getInstance();
            System.out.println("Connection successful");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
