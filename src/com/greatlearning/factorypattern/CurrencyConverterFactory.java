package com.greatlearning.factorypattern;

public class CurrencyConverterFactory {

    public double convertCurrency(String currency, double amount) throws Exception {
        return getCurrencyConverter(currency).convertToINR(amount);
    }

    public CurrencyConverter getCurrencyConverter(String currency) throws Exception {
        switch (currency) {
            case "USD":
                return new DollarConverter();
            case "GBP":
                return new GBPConverter();
            default:
                throw new Exception("Please select valid currency !");
        }
    }
}
