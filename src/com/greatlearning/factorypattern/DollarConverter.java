package com.greatlearning.factorypattern;

public class DollarConverter implements CurrencyConverter {

    public static final double USD_PRICE = 74.20;

    @Override
    public double convertToINR(double amount) {
        return amount * USD_PRICE;
    }
}
