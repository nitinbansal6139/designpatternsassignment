package com.greatlearning.factorypattern;

public class GBPConverter implements CurrencyConverter {

    public static final double GBP_PRICE = 101.82;

    @Override
    public double convertToINR(double amount) {
        return amount * GBP_PRICE;
    }
}
