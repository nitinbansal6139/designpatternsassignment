package com.greatlearning.factorypattern;

public interface CurrencyConverter {
    double convertToINR(double amount);
}
