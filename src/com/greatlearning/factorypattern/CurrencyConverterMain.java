package com.greatlearning.factorypattern;

public class CurrencyConverterMain {

    public static void main(String[] args) throws Exception {
        CurrencyConverterFactory currencyConverterFactory = new CurrencyConverterFactory();
        System.out.println("500 GBP = " + currencyConverterFactory.convertCurrency("GBP", 500) + " INR");
        System.out.println("700 USD = " + currencyConverterFactory.convertCurrency("USD", 700) + " INR");
    }

}
