package com.greatlearning.builderpattern;

public class EmiSchedule {

    private double amount;
    private int numberOfEMIs;

    public EmiSchedule(double amount, int numberOfEMIs) {
        this.amount = amount;
        this.numberOfEMIs = numberOfEMIs;
    }

    @Override
    public String toString() {
        return "EmiSchedule{" +
                "amount=" + amount +
                ", numberOfEMIs=" + numberOfEMIs +
                '}';
    }
}
