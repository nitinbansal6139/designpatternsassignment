package com.greatlearning.builderpattern;

public class AtmTransaction {

    private double withdrawalAmt;
    private double depositAmt;

    public AtmTransaction(double withdrawalAmt, double depositAmt) {
        this.withdrawalAmt = withdrawalAmt;
        this.depositAmt = depositAmt;
    }

    @Override
    public String toString() {
        return "AtmTransaction{" +
                "withdrawalAmt=" + withdrawalAmt +
                ", depositAmt=" + depositAmt +
                '}';
    }
}
