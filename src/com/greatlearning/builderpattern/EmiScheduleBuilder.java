package com.greatlearning.builderpattern;

public class EmiScheduleBuilder {

    private double amount;
    private int numberOfEMIs;

    public EmiScheduleBuilder setAmount(double amount) {
        this.amount = amount;
        return this;
    }

    public EmiScheduleBuilder setNumberOfEMIs(int numberOfEMIs) {
        this.numberOfEMIs = numberOfEMIs;
        return this;
    }

    public EmiSchedule createEmiScheduler() {
        return new EmiSchedule(amount, numberOfEMIs);
    }
}
