package com.greatlearning.builderpattern;

public class BankAccountBuilder {

    private String bankAccountNo;
    private String accountType;
    private String branch;
    private double balance;
    private AtmTransaction atmTransaction;
    private EmiSchedule emiSchedule;

    public BankAccountBuilder setBankAccountNo(String bankAccountNo) {
        this.bankAccountNo = bankAccountNo;
        return this;
    }

    public BankAccountBuilder setAccountType(String accountType) {
        this.accountType = accountType;
        return this;
    }

    public BankAccountBuilder setBranch(String branch) {
        this.branch = branch;
        return this;
    }

    public BankAccountBuilder setBalance(double balance) {
        this.balance = balance;
        return this;
    }

    public BankAccountBuilder setAtmTransactions(AtmTransaction atmTransaction) {
        this.atmTransaction = atmTransaction;
        return this;
    }

    public BankAccountBuilder setEmiSchedule(EmiSchedule emiSchedule) {
        this.emiSchedule = emiSchedule;
        return this;
    }

    public BankAccount createBankAccount() {
        return new BankAccount(bankAccountNo, accountType, branch, balance, atmTransaction, emiSchedule);
    }
}
