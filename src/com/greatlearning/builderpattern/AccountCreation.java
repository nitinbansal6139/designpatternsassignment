package com.greatlearning.builderpattern;

public class AccountCreation {

    public static void main(String[] args) {
        BankAccount bankAccountSavings = new BankAccountBuilder()
                .setBankAccountNo("101")
                .setAccountType("Savings")
                .setBranch("Whitefield,Bangalore")
                .setBalance(100)
                .setAtmTransactions(new AtmTransactionsBuilder()
                        .setDepositAmt(1000)
                        .setWithdrawalAmt(200)
                        .createAtmTransaction())
                .setEmiSchedule(new EmiScheduleBuilder()
                        .setNumberOfEMIs(5)
                        .setAmount(500.20)
                        .createEmiScheduler())
                .createBankAccount();
        System.out.println("Savings bank account details : " + bankAccountSavings);

        BankAccount bankAccountCurrent = new BankAccountBuilder()
                .setBankAccountNo("102")
                .setAccountType("Current")
                .setBranch("Koramangala,Bangalore")
                .setBalance(1000)
                .setAtmTransactions(new AtmTransactionsBuilder()
                        .setDepositAmt(2000)
                        .setWithdrawalAmt(400)
                        .createAtmTransaction())
                .setEmiSchedule(new EmiScheduleBuilder()
                        .setNumberOfEMIs(3)
                        .setAmount(200.20)
                        .createEmiScheduler())
                .createBankAccount();
        System.out.println("Current bank account details : " + bankAccountCurrent);
    }

}
