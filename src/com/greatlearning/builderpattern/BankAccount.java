package com.greatlearning.builderpattern;

public class BankAccount {

    private String bankAccountNo;
    private String accountType;
    private String branch;
    private double balance;
    private AtmTransaction atmTransaction;
    private EmiSchedule emiSchedule;

    public BankAccount(String bankAccountNo, String accountType, String branch, double balance, AtmTransaction atmTransaction, EmiSchedule emiSchedule) {
        this.bankAccountNo = bankAccountNo;
        this.accountType = accountType;
        this.branch = branch;
        this.balance = balance;
        this.atmTransaction = atmTransaction;
        this.emiSchedule = emiSchedule;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "bankAccountNo='" + bankAccountNo + '\'' +
                ", accountType='" + accountType + '\'' +
                ", branch='" + branch + '\'' +
                ", balance=" + balance +
                ", atmTransaction=" + atmTransaction +
                ", emiSchedule=" + emiSchedule +
                '}';
    }
}
