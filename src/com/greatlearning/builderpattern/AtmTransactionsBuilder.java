package com.greatlearning.builderpattern;

public class AtmTransactionsBuilder {
    private int withdrawalAmt;
    private int depositAmt;

    public AtmTransactionsBuilder setWithdrawalAmt(int withdrawalAmt) {
        this.withdrawalAmt = withdrawalAmt;
        return this;
    }

    public AtmTransactionsBuilder setDepositAmt(int depositAmt) {
        this.depositAmt = depositAmt;
        return this;
    }

    public AtmTransaction createAtmTransaction() {
        return new AtmTransaction(withdrawalAmt, depositAmt);
    }
}
